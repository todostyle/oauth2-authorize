package com.xaaef.authorize.client;

import com.xaaef.authorize.common.domain.TokenValue;
import com.xaaef.authorize.common.enums.GrantType;
import com.xaaef.authorize.common.enums.OAuth2Error;
import com.xaaef.authorize.common.exception.OAuth2Exception;
import com.xaaef.authorize.common.util.JsonResult;
import com.xaaef.authorize.common.util.JsonUtils;
import com.xaaef.authorize.common.util.SecurityUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * All rights Reserved, Designed By www.xaaef.com
 * <p>
 * 只有 使用了 spring mvc 才会注入，这个类
 * </p>
 *
 * @author Wang Chen Chen
 * @version 1.0.1
 * @date 2021/7/13 17:29
 * @copyright 2021 http://www.xaaef.com Inc. All rights reserved.
 */

@Slf4j
@Configuration
@AllArgsConstructor
@ConditionalOnClass(org.springframework.web.servlet.HandlerInterceptor.class)
public class OAuth2ClientInterceptor implements HandlerInterceptor, WebMvcConfigurer {

    private AuthorizeService authorizeService;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this)
                .addPathPatterns("/**");
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 此路径忽略
        if (authorizeService.urlIgnore(request.getRequestURI())) {
            return true;
        }
        var props = authorizeService.getProps();
        // 从请求头中，获取 TokenId
        var tokenId = request.getHeader(props.getTokenIdHeader());
        TokenValue tokenValue = null;
        // 如果当前请求，经过网关过滤，就一定会携带 TokenId 请求头
        if (StringUtils.hasText(tokenId)) {
            tokenValue = authorizeService.get(tokenId);
        } else {
            // 如果没有 携带 TokenId 请求头。那么就获取 Authorization 当请求头参数。
            var bearerToken = request.getHeader(props.getTokenHeader());
            if (StringUtils.hasText(bearerToken)) {
                try {
                    // 发送请求到服务器。校验 jwt token 的有效性
                    tokenValue = authorizeService.validate(bearerToken);
                } catch (OAuth2Exception e) {
                    error(response, e);
                    return false;
                }
            } else {
                error(response, new OAuth2Exception(OAuth2Error.ACCESS_TOKEN_INVALID));
                return false;
            }
        }

        // 忽略 openFeign 的 客户端 和 用户类型 校验
        String feign = request.getHeader(props.getFeignHeader());
        if (props.isIgnoreFeign() && props.getFeignHeader().equals(feign)) {
            // 将 TokenValue 设置到 ThreadLocal 中！
            SecurityUtils.setTokenValue(tokenValue);
            return true;
        }

        // 判断 token 的认证方式，如果是 客户端模式，那么就没有用户信息。
        if (props.isUserAuth() && tokenValue.getGrantType() == GrantType.CLIENT_CREDENTIALS) {
            error(response, new OAuth2Exception(OAuth2Error.TOKEN_USER_INVALID));
            return false;
        }

        // 校验 用户类型
        if (tokenValue.getUser().getUserType() != props.getUserType()) {
            error(response, new OAuth2Exception(OAuth2Error.USER_TYPES_ERROR.getStatus(),
                    String.format("此路径需要,用户类型为 %s 才能调用！", props.getUserType().getDescription()))
            );
            return false;
        }

        // 将 TokenValue 设置到 ThreadLocal 中！
        SecurityUtils.setTokenValue(tokenValue);
        return true;
    }

    /**
     * 将字符串渲染到客户端
     *
     * @param response 渲染对象
     * @param ex       待渲染的字符串
     * @return null
     */
    private static String error(HttpServletResponse response, OAuth2Exception ex) {
        try {
            response.setStatus(HttpStatus.OK.value());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
            String json = JsonUtils.toJson(JsonResult.error(ex.getStatus(), ex.getMessage()));
            response.getWriter().print(json);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return null;
    }

}
