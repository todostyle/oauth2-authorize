package com.xaaef.authorize.common.domain;

import com.xaaef.authorize.common.enums.GrantType;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * All rights Reserved, Designed By www.xaaef.com
 * <p>
 * 登录成功的客户端，用户信息
 * </p>
 *
 * @author Wang Chen Chen
 * @version 1.0.1
 * @date 2021/7/12 12:08
 * @copyright 2021 http://www.xaaef.com Inc. All rights reserved.
 */

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TokenValue implements Serializable {

    /**
     * 全局唯一认证 Id
     */
    private String tokenId;

    /**
     * 认证授权方式
     */
    private GrantType grantType;

    /**
     * 用户信息
     */
    private UserInfo user;

    /**
     * 客户端信息
     */
    private ClientDetails client;

    /**
     * 登录时间
     */
    private LocalDateTime loginTime;


}
