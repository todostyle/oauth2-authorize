package com.xaaef.authorize.common.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.xaaef.authorize.common.enums.AdminFlag;
import com.xaaef.authorize.common.enums.GenderType;
import com.xaaef.authorize.common.enums.UserStatus;
import com.xaaef.authorize.common.enums.UserType;
import lombok.*;

import java.io.Serializable;

/**
 * All rights Reserved, Designed By www.xaaef.com
 * <p>
 * 用户信息 详情
 * </p>
 *
 * @author Wang Chen Chen
 * @version 1.0.1
 * @date 2021/7/12 12:08
 * @copyright 2021 http://www.xaaef.com Inc. All rights reserved.
 */

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo implements Serializable {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 用户名
     */
    private String username;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户名称
     */
    private String nickname;

    /**
     * 密码
     */
    @JsonIgnore
    private String password;

    /**
     * 性别[ 0.女  1.男  2.未知]
     *
     * @author Wang Chen Chen
     * @date 2021/7/12 13:41
     */
    private GenderType gender;

    /**
     * 用户类型，租户用户，系统用户
     *
     * @author Wang Chen Chen
     * @date 2021/7/12 13:41
     */
    private UserType userType;

    /**
     * 状态
     */
    private UserStatus status;

    /**
     * 当前用户是不是管理员
     */
    private AdminFlag adminFlag;


}
