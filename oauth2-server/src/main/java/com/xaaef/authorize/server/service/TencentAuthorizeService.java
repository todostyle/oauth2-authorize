package com.xaaef.authorize.server.service;

import com.xaaef.authorize.common.domain.UserInfo;
import com.xaaef.authorize.common.exception.OAuth2Exception;
import com.xaaef.authorize.server.params.SmsModeParam;
import com.xaaef.authorize.server.params.TencentModeParam;
import com.xaaef.authorize.server.token.OAuth2Token;

/**
 * All rights Reserved, Designed By www.xaaef.com
 * <p>
 * 腾讯QQ，和 微信 授权服务
 * </p>
 *
 * @author Wang Chen Chen
 * @version 1.0.1
 * @date 2021/7/12 14:21
 * @copyright 2021 http://www.xaaef.com Inc. All rights reserved.
 */

public interface TencentAuthorizeService {

    /**
     * 构建 微信授权地址
     *
     * @param param
     * @author Wang Chen Chen
     * @date 2021/7/12 14:23
     */
    String buildWeChatAuthAddress(TencentModeParam param) throws OAuth2Exception;


    /**
     * 构建 QQ授权地址
     *
     * @param param
     * @author Wang Chen Chen
     * @date 2021/7/12 14:23
     */
    String buildQQAuthAddress(TencentModeParam param) throws OAuth2Exception;


    /**
     * 腾讯QQ 认证模式
     *
     * @param code
     * @param state
     * @return OAuth2Token
     * @throws OAuth2Exception
     * @author Wang Chen Chen<932560435@qq.com>
     * @date 2020/7/23 16:48
     */
    OAuth2Token QqAuthorize(String code, String state) throws OAuth2Exception;

}
