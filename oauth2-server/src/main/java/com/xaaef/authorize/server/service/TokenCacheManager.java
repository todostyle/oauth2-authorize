package com.xaaef.authorize.server.service;

import com.xaaef.authorize.common.domain.ClientDetails;
import com.xaaef.authorize.common.domain.TokenValue;
import com.xaaef.authorize.common.domain.UserInfo;
import com.xaaef.authorize.server.params.GetCodeModeParam;
import com.xaaef.authorize.server.params.TencentModeParam;

import java.util.concurrent.TimeUnit;

/**
 * All rights Reserved, Designed By 深圳市铭灏天智能照明设备有限公司
 * <p>
 * 服务端 token 缓存管理器
 * </p>
 *
 * @author Wang Chen Chen
 * @version 1.0.1
 * @date 2021/8/3 15:34
 * @copyright 2021 http://www.mhtled.com Inc. All rights reserved.
 */

public interface TokenCacheManager {

    /**
     * 根据 tokenId 获取 AccessToken
     *
     * @param tokenId
     * @return TokenValue
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    TokenValue getAccessToken(String tokenId);


    /**
     * 设置 AccessToken
     *
     * @param tokenValue
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void setAccessToken(String tokenId, TokenValue tokenValue);


    /**
     * 移除 AccessToken
     *
     * @param tokenId
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void removeAccessToken(String tokenId);


    /**
     * 根据 tokenId 获取 RefreshToken
     *
     * @param tokenId
     * @return TokenValue
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    TokenValue getRefreshToken(String tokenId);


    /**
     * 设置 RefreshToken
     *
     * @param tokenValue
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void setRefreshToken(String tokenId, TokenValue tokenValue);


    /**
     * 移除 RefreshToken
     *
     * @param tokenId
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void removeRefreshToken(String tokenId);


    /**
     * 根据 key 获取 GetCodeModeParam 值
     * <p>
     * 授权码模式下。当第三方客户端第一次发起请求，保存请求参数。返回 code 给第三方
     * 当第三方拿着code来换取AccessToken时，校验两次的客户端是否一直
     *
     * @param codeKey
     * @return GetCodeModeParam
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    GetCodeModeParam getCodeMode(String codeKey);


    /**
     * 设置 GetCodeModeParam 值
     *
     * @param param
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void setCodeMode(String codeKey, GetCodeModeParam param);


    /**
     * 移除 GetCodeModeParam
     *
     * @param codeKey
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void removeCodeMode(String codeKey);


    /**
     * 根据 code 获取 登录的用户信息
     * <p>
     * 授权码模式下。当第三方客户端通过 code 获取 AccessToken 时，
     * 获取登录的用户信息
     *
     * @param code
     * @return GetCodeModeParam
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    UserInfo getLoginUser(String code);


    /**
     * 设置 loginUser 值
     *
     * @param code
     * @param loginUser
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void setLoginUser(String code, UserInfo loginUser);


    /**
     * 移除 loginUser 值
     *
     * @param code
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void removeLoginUser(String code);


    /**
     * 获取 String 值
     *
     * @param key
     * @return String
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    String getString(String key);


    /**
     * 设置 String 值
     *
     * @param key
     * @param value
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void setString(String key, String value, long timeout, TimeUnit unit);


    /**
     * 移除 String 值
     *
     * @param key
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void removeString(String key);


    /**
     * 获取 在线用户 的 tokenId
     *
     * @param username
     * @return String 用户名称
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    String getOnlineUser(String username);


    /**
     * 设置 在线用户名 tokenId
     *
     * @param tokenId
     * @param username
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void setOnlineUser(String username, String tokenId);


    /**
     * 移除 String 值
     *
     * @param key
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void removeOnlineUser(String username);


    /**
     * 获取 被强制挤下线的用户
     *
     * @param tokenId
     * @return String 用户名称
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    String getForcedOffline(String tokenId);


    /**
     * 设置 被强制挤下线的用户 离线时间
     *
     * @param tokenId
     * @param offlineTime
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void setForcedOffline(String tokenId, String offlineTime);


    /**
     * 移除 被强制挤下线的用户
     *
     * @param tokenId
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void removeForcedOffline(String tokenId);


    /**
     * 获取 TencentModeParam 详情
     *
     * @param state
     * @return GetCodeModeParam
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    TencentModeParam getTencentMode(String state);


    /**
     * 设置 TencentModeParam 值
     *
     * @param param
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void setTencentMode(String state, TencentModeParam param);


    /**
     * 移除 TencentModeParam
     *
     * @param state
     * @author Wang Chen Chen
     * @date 2021/7/12 16:29
     */
    void removeTencentMode(String state);

}
