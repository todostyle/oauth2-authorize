package com.xaaef.authorize.server.service.impl;

import com.xaaef.authorize.common.domain.ClientDetails;
import com.xaaef.authorize.common.domain.TokenValue;
import com.xaaef.authorize.common.domain.UserInfo;
import com.xaaef.authorize.common.enums.GrantType;
import com.xaaef.authorize.common.enums.OAuth2Error;
import com.xaaef.authorize.common.exception.OAuth2Exception;
import com.xaaef.authorize.server.service.CodeAuthorizeService;
import com.xaaef.authorize.server.token.OAuth2Token;
import com.xaaef.authorize.server.params.AuthorizationCodeModeParam;
import com.xaaef.authorize.server.params.GetCodeModeParam;
import com.xaaef.authorize.server.params.LoginParam;
import com.xaaef.authorize.server.repository.ClientDetailsRepository;
import com.xaaef.authorize.server.repository.UserInfoRepository;
import com.xaaef.authorize.server.service.TokenCacheManager;
import com.xaaef.authorize.server.util.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import static com.xaaef.authorize.server.constant.TokenConstant.*;

/**
 * All rights Reserved, Designed By www.xaaef.com
 * <p>
 * </p>
 *
 * @author Wang Chen Chen<932560435@qq.com>
 * @version 1.0.1
 * @date 2021/7/17 16:49
 * @copyright 2021 http://www.xaaef.com Inc. All rights reserved.
 */

@Slf4j
@Service
public class CodeAuthorizeServiceImpl extends DefaultAuthorizeService implements CodeAuthorizeService {

    public CodeAuthorizeServiceImpl(ClientDetailsRepository clientDetailsRepository,
                                    UserInfoRepository userInfoRepository,
                                    TokenCacheManager cacheManager,
                                    JwtTokenUtils jwtTokenUtils) {
        super(clientDetailsRepository, userInfoRepository, cacheManager, jwtTokenUtils);
    }

    @Override
    public String validateClient(GetCodeModeParam param) throws OAuth2Exception {
        ClientDetails client = getClient(param.getClientId());
        // 判断 客户端 是否存在
        if (client == null) {
            throw new OAuth2Exception(OAuth2Error.CLIENT_INVALID);
        }
        // 校验授权方式,是否 code
        if (!StringUtils.equalsIgnoreCase(GrantType.CODE.getCode(), param.getResponseType())) {
            throw new OAuth2Exception(OAuth2Error.AUTHORIZATION_GRANT_TYPE);
        }
        // 校验此客户端，是否包含 authorization_code 模式
        if (!client.containsGrantType(GrantType.AUTHORIZATION_CODE.getCode())) {
            throw new OAuth2Exception(OAuth2Error.AUTHORIZATION_GRANT_TYPE);
        }
        // 将 第三方 传入的 redirectUri 进行解码
        var decodeRedirectUri = URLDecoder.decode(param.getRedirectUri(), StandardCharsets.UTF_8);
        var redirectUri = URI.create(decodeRedirectUri);
        // 比较 第三方传入的 redirectUri 域名部分 和 数据的 域名部分 是否一致
        if (!StringUtils.equalsIgnoreCase(redirectUri.getHost(), client.getDomainName())) {
            throw new OAuth2Exception(OAuth2Error.DOMAIN_NAME_ILLEGAL);
        }
        param.setRedirectUri(decodeRedirectUri);

        // 一个随机的 授权ID，由系统随机生成，绑定给每个前来授权的第三方应用，
        // 主要用来连贯用户一些列操作！
        String codeId = jwtTokenUtils.createUUID();

        // 先将授权码的参数，保存到缓存中，以便后面使用
        cacheManager.setCodeMode(codeId, param);

        return codeId;
    }

    @Override
    public String login(LoginParam param) throws OAuth2Exception {
        GetCodeModeParam codeMode = cacheManager.getCodeMode(param.getCodeId());
        if (codeMode == null) {
            throw new OAuth2Exception(OAuth2Error.CLIENT_INVALID);
        }
        // 校验用户信息，
        UserInfo userInfo = validateUserInfo(param.getUsername(), param.getPassword());
        // 生成随机 code 返回给第三方应用，就可以通过 code 来获取 access_token！
        String code = jwtTokenUtils.createUUID();

        // 将 code 关联 登录的用户信息，保存起来。
        cacheManager.setLoginUser(code, userInfo);

        // 移出上一步缓存的 授权码参数
        cacheManager.removeCodeMode(param.getCodeId());

        // 将第一步中 客户端id，跟 code 绑定。在下一步获取 access_token 校验两次 客户端是否一致
        cacheManager.setString(CODE_CLIENT_ID_KEY + code,
                codeMode.getClientId(),
                getProps().getCodeExpired(),
                TimeUnit.SECONDS
        );

        // 构建回调 url 。跳转到第三方应用
        return builderRedirectUri(codeMode, code);
    }

    /**
     * 拼接 回调 url
     *
     * @author Wang Chen Chen<932560435@qq.com>
     * @create 2021/7/17 17:24
     */
    public String builderRedirectUri(GetCodeModeParam codeMode, String code) {
        /**
         * 判断url是否已经携带参数了
         * 如：https://xaaef.com/authorize/callback?pid=2135543
         * 就只能在后面继续加 &
         * 如：https://xaaef.com/authorize/callback
         * 就需要加 ?
         * */
        var prefix = "?";
        if (StringUtils.contains(codeMode.getRedirectUri(), prefix)) {
            prefix = "&";
        }
        // 构建回调 url
        return new StringBuilder(codeMode.getRedirectUri())
                .append(prefix)
                .append("code=").append(code)
                .append("&state=").append(StringUtils.trimToEmpty(codeMode.getState()))
                .toString();
    }


    @Override
    public OAuth2Token authorize(AuthorizationCodeModeParam param) throws OAuth2Exception {
        ClientDetails client = validateClient(
                param.getClientId(),
                param.getClientSecret(),
                GrantType.AUTHORIZATION_CODE,
                param.getGrantType()
        );

        // 获取 getCode 时使用的 客户端id
        String codeClientIdKey = CODE_CLIENT_ID_KEY + param.getCode();

        // 授权码模式，第一步中的客户端ID
        String clientId = cacheManager.getString(codeClientIdKey);

        // 比较两次客户端是否一致
        if (!StringUtils.equals(clientId, param.getClientId())) {
            throw new OAuth2Exception(OAuth2Error.CLIENT_IS_DIFFERENT);
        }

        // 通过 code 获取 前面保存的 登录用户的信息
        UserInfo userInfo = cacheManager.getLoginUser(param.getCode());
        if (userInfo == null) {
            throw new OAuth2Exception(OAuth2Error.USER_INVALID);
        }

        // 生成 唯一的 认证id
        String tokenId = jwtTokenUtils.createTokenId();

        // 服务端 token
        TokenValue tokenValue = TokenValue.builder()
                .tokenId(tokenId)
                .client(client)
                .user(userInfo)
                .grantType(GrantType.AUTHORIZATION_CODE)
                .build();

        setTokenCache(tokenId, tokenValue);

        // 删除存储的信息
        cacheManager.removeString(codeClientIdKey);

        cacheManager.removeLoginUser(param.getCode());

        return buildToken(tokenId, client.getScope());
    }

}
