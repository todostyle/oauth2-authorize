package com.xaaef.authorize.server.service.impl;

import com.xaaef.authorize.common.domain.TokenValue;
import com.xaaef.authorize.common.enums.GrantType;
import com.xaaef.authorize.common.enums.OAuth2Error;
import com.xaaef.authorize.common.exception.OAuth2Exception;
import com.xaaef.authorize.server.token.OAuth2Token;
import com.xaaef.authorize.common.util.SecurityUtils;
import com.xaaef.authorize.server.props.OAuth2ServerProperties;
import com.xaaef.authorize.server.repository.ClientDetailsRepository;
import com.xaaef.authorize.server.repository.UserInfoRepository;
import com.xaaef.authorize.server.service.TokenCacheManager;
import com.xaaef.authorize.server.service.TokenService;
import com.xaaef.authorize.server.util.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * All rights Reserved, Designed By www.xaaef.com
 * <p>
 * </p>
 *
 * @author Wang Chen Chen
 * @version 1.0.1
 * @date 2021/7/13 10:01
 * @copyright 2021 http://www.xaaef.com Inc. All rights reserved.
 */


@Slf4j
@Service
public class TokenServiceImpl extends DefaultAuthorizeService implements TokenService {

    public TokenServiceImpl(ClientDetailsRepository clientDetailsRepository,
                            UserInfoRepository userInfoRepository,
                            TokenCacheManager cacheManager,
                            JwtTokenUtils jwtTokenUtils) {
        super(clientDetailsRepository, userInfoRepository, cacheManager, jwtTokenUtils);
    }

    @Override
    public TokenValue validate(String bearerToken) throws OAuth2Exception {
        String tokenId = jwtTokenUtils.getIdFromToken(bearerToken);
        TokenValue tokenValue = cacheManager.getAccessToken(tokenId);
        if (tokenValue == null || tokenValue.getClient() == null) {
            OAuth2ServerProperties props = jwtTokenUtils.getProps();
            // 判断是否启用单点登录
            if (props.getSso()) {
                // 判断此用户，是不是被挤下线
                String offlineTime = cacheManager.getForcedOffline(tokenId);
                if (StringUtils.hasText(offlineTime)) {
                    // 删除 被挤下线 的消息提示
                    cacheManager.removeForcedOffline(tokenId);
                    String errMsg = String.format("您的账号在[ %s ]被其他用户拥下线了！", offlineTime);
                    log.debug("errMsg {}", errMsg);
                    throw new OAuth2Exception(errMsg);
                }
            }
            throw new OAuth2Exception("当前登录用户不存在!");
        }
        return tokenValue;
    }

    @Override
    public boolean urlIgnore(String url) {
        OAuth2ServerProperties props = jwtTokenUtils.getProps();
        return SecurityUtils.urlIgnore(props.getExcludePath(), url);
    }

    @Override
    public OAuth2Token refresh(String bearerToken) throws OAuth2Exception {
        String refreshTokenId = jwtTokenUtils.getIdFromToken(bearerToken);
        TokenValue refreshToken = cacheManager.getRefreshToken(refreshTokenId);
        if (refreshToken == null) {
            throw new OAuth2Exception(OAuth2Error.REFRESH_TOKEN_INVALID);
        }

        // 生成 唯一的 认证id
        String tokenId = jwtTokenUtils.createTokenId();

        setTokenCache(tokenId, refreshToken);

        // 移除 旧的 access_token, refresh_token
        cacheManager.removeRefreshToken(refreshTokenId);
        cacheManager.removeAccessToken(refreshTokenId);
        cacheManager.removeForcedOffline(refreshTokenId);

        return buildToken(tokenId, refreshToken.getClient().getScope());
    }


    @Override
    public void logout() {
        String tokenId = SecurityUtils.getTokenId();
        // 移除 Token
        cacheManager.removeAccessToken(tokenId);
        cacheManager.removeRefreshToken(tokenId);
        if (SecurityUtils.getTokenValue().getGrantType() != GrantType.CLIENT_CREDENTIALS) {
            if (SecurityUtils.getUserInfo() != null) {
                // 移除 在线用户
                cacheManager.removeOnlineUser(SecurityUtils.getUserInfo().getUsername());
            }
            // 移除 强制下线
            cacheManager.removeForcedOffline(tokenId);
        }
    }

}
