/*
 Navicat Premium Data Transfer

 Source Server         : aliyun-mysql-8
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-wz9u8t02hg3s52482io.mysql.rds.aliyuncs.com:3306
 Source Schema         : oauth2_server

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 03/08/2021 17:14:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for client_details
-- ----------------------------
DROP TABLE IF EXISTS `client_details`;
CREATE TABLE `client_details`  (
  `client_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Client Id',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '商户ID',
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '客户端 密钥',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '客户端名称',
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '客户端 图标',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '描述',
  `client_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '客户端类型，0.第三方应用  1.内部应用',
  `grant_types` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '授权类型 json数组格式 [\"sms\",\"password\"]',
  `domain_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '域名地址，如果是 授权码模式，\r\n必须校验回调地址是否属于此域名之下',
  `scope` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '授权作用域',
  `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 【0.否 1.是】',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `create_user` bigint(20) NOT NULL COMMENT '创建人',
  `last_update_time` datetime NOT NULL COMMENT '最后一次，修改时间',
  `last_update_user` bigint(20) NOT NULL COMMENT '最后一次，修改人',
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '[ OAuth2.0 ] 客户端详情' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of client_details
-- ----------------------------
INSERT INTO `client_details` VALUES ('7KutwpFgFXv0hcvkBO', 'root', '$2a$10$lLD53J3fk64uzV9Ti3h/f.J8u5UnZ4Xm7TeM/QLUKhM88lEdUMg6i', '商户管理后台', 'dwa', '多商户管理后台', 1, '[\"we_chat\",\"password\",\"tencent_qq\",\"sms\"]', 'www.xaaef.com', 'read,write', 0, '2021-07-12 17:59:08', 609429132107067392, '2021-07-12 17:59:16', 609429132107067392);
INSERT INTO `client_details` VALUES ('nssbTtp5FO6NjZpUwP', 'root', '$2a$10$5pXbuKL0l6H.bdc4ud8BCeuLkrzfys0SunrdouwPTKmKuO9DbJk5m', 'vue.js总部管理后台', 'dwa', 'vue.js总部管理后台', 1, '[\"*\"]', 'www.xaaef.com', 'read,write', 0, '2021-07-12 17:59:08', 609429132107067392, '2021-07-12 17:59:16', 609429132107067392);
INSERT INTO `client_details` VALUES ('VIUvXZmVXmOFh1gYWK', 'kgnuY1HC65ZaolcT7d', '$2a$10$yUa8IAmb4nC48jnmv0OuAu6zg90bKsHfqhxFNN7kYc83b7sJNL.pC', '华为IOT园区项目组,API', 'dwa', '华为IOT园区项目组,API', 0, '[\"client_credentials\"]', 'www.huawei.com', 'read', 0, '2021-07-12 17:59:08', 609429132107067392, '2021-07-12 17:59:16', 609429132107067392);

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '头像',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商户ID',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账号',
  `mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号码',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邮箱',
  `nickname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名称',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `gender` tinyint(4) NOT NULL COMMENT '性别[ 0.女  1.男  2.未知]',
  `birthday` date NOT NULL COMMENT '生日',
  `status` tinyint(1) NOT NULL COMMENT '状态 【0.禁用 1.正常 2.锁定 】',
  `user_type` tinyint(1) NOT NULL COMMENT '用户类型 0.租户用户 1. 系统用户 ',
  `admin_flag` tinyint(1) NOT NULL COMMENT '0. 普通用户  1. 管理员',
  `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 【0.否 1.是】',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_user` bigint(20) NOT NULL COMMENT '创建人',
  `last_update_time` datetime NOT NULL COMMENT '最后一次修改时间',
  `last_update_user` bigint(20) NOT NULL COMMENT '最后一次修改人',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `UK_ulo5s2i7qoksp54tgwl_mobile`(`mobile`) USING BTREE,
  UNIQUE INDEX `UK_6i5ixxulo5s2i7qoksp54tgwl_username`(`username`) USING BTREE,
  UNIQUE INDEX `UK_6i5ixxulo5s2i7984erhgwl_email`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '[ 权限 ] 用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES (609429132107067392, 'https://tl329pszs0mg.oss-cn-shenzhen.aliyuncs.com/avatar/20210728105923.png', 'root', 'admin', '15071525233', 'admin@qq.com', '管理员', '$2a$10$nslLpuye4W43EcWHIhzBEekSWKlSctKgpmVWFFysz157YCVhlOv52', 1, '1995-08-19', 1, 1, 1, 0, '2021-05-14 11:17:33', 609429132107067392, '2021-07-27 14:10:29', 609429132107067392);
INSERT INTO `user_info` VALUES (609429132107067394, 'https://images.xaaef.com/20210719104730.jpg', 'root', 'doudou', '15071525235', 'doudou@.com', '开发用户', '$2a$10$BynoLxqTC0d6WZRnSfmRLeBTtsdM0XZ/rWkmPX.vepX5Y5t0KTF.2', 1, '1995-08-17', 1, 1, 1, 0, '2021-05-14 11:17:33', 609429132107067392, '2021-05-14 11:17:36', 609429132107067392);
INSERT INTO `user_info` VALUES (614504679430434816, 'https://tl329pszs0mg.oss-cn-shenzhen.aliyuncs.com/avatar/20210728105923.png', '05c5d014e78febfcb63622d8204b7aab', 'P0iGErtApDcUnMnQ', '15071533365', 'guangzoubaoli@163.com', '广州保利集团', '$2a$10$x0qA6q7MwNorCji..ubHTeWsFYg/NNPRYMQLR7z27czmmTBbRizRS', 1, '1998-08-17', 1, 0, 1, 0, '2021-07-19 14:23:18', 609429132107067394, '2021-07-19 14:23:18', 609429132107067394);
INSERT INTO `user_info` VALUES (1420588891812712449, 'https://tl329pszs0mg.oss-cn-shenzhen.aliyuncs.com/image/mhtled_logo.png', '6427aa53e0305f878b29677945ba3404', 'tiananyungu', '15675195538', 'taygadmin@qq.com', '天安云谷-管理员', '$2a$10$89C8032ne.GK01dbu/k94ONx3qw.ykrW.HFHJmav5ijOkmSMy5dYe', 1, '1998-08-17', 1, 0, 1, 0, '2021-07-29 11:36:00', 609429132107067394, '2021-07-29 11:36:00', 609429132107067394);
INSERT INTO `user_info` VALUES (1420655828613820418, 'https://tl329pszs0mg.oss-cn-shenzhen.aliyuncs.com/avatar/20210728105923.png', '05c5d014e78febfcb63622d8204b7aab', 'baolitest', '15869149884', 'baolitest@qq.com', '保利测试', '$2a$10$D5zIIeMAeHqfKB9v4DOrMO2oM0f.Mn/s.Vb3dmwk7HNDHEY2YOqki', 1, '1998-08-17', 1, 0, 0, 0, '2021-07-29 16:01:59', 614504679430434816, '2021-07-29 16:01:59', 614504679430434816);
INSERT INTO `user_info` VALUES (1420658800659836930, 'https://tl329pszs0mg.oss-cn-shenzhen.aliyuncs.com/image/mhtled_logo.png', '16a27e119ebbaf8435aa46aa43b5f24b', 'lvxie', '15675195888', 'lvxie@qq.com', '绿协-管理员', '$2a$10$kDbs0dHEpwASq659.uTu1.8yCbVCfZDA9wnpFsNP3Fv343gRNILGu', 1, '1998-08-17', 1, 0, 1, 0, '2021-07-29 16:13:47', 609429132107067394, '2021-07-29 16:13:47', 609429132107067394);

-- ----------------------------
-- Table structure for user_social
-- ----------------------------
DROP TABLE IF EXISTS `user_social`;
CREATE TABLE `user_social`  (
  `social_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户社交ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户唯一ID',
  `open_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '社交账号唯一ID',
  `social_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'we_chat. 微信  tencent_qq. 腾讯QQ',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`social_id`) USING BTREE,
  UNIQUE INDEX `UK_sfewfe49823_open_id`(`open_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '[ 系统 ] 用户社交平台登录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_social
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
